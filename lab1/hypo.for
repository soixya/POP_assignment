C FIND THE THIRD SIDE OF RIGHT ANGLE TRIANGLE 
C PERPENDICULAR AND BASE GIVEN 		

		PROGRAM SOFTRI
		REAL B,P,H
		WRITE(*,*)'ENTER P AND B'
C 		reads the value of base and perpendicular given by user
		READ *, B,P
C 		calling subroutine CALCHYP which passes attribute P, B, and H 
		CALL CALCHYP(P,B,H)
		WRITE(*,*)  'H:', H
		END

C 		defining subroutine CALCHYP which has parameter X, Y, and OUTPUT 
		SUBROUTINE CALCHYP(X,Y,OUTPUT)
		REAL X,Y,OUTPUT
		OUTPUT = SQRT(X**2 + Y**2)
		RETURN
		END	