C       ARITHMETIC MEAN, GEOMETRIC MEAN AND HARMONIC MEAN OF THREE NUMBERS       
        PROGRAM MEANS
C define variable of type real        
        REAL A, B, C
C assigning value to each variable        
        A = 10          
        B = 20
        C = 30
C calculating airthmetic mean, geometric mean and harmonic mean
        AM = (A + B +C)/3.0
        GM = (A*B*C)**(1.0/3.0)
        HM = 3.0/(1.0/A + 1.0/B + 1.0/C)
C for writing the value of each mean we use write command
        WRITE(*,*) 'The Arithmetic Mean is', AM
        WRITE(*,*) 'The Geometric Mean is', GM
        WRITE(*,*) 'The Harmonic Mean is', HM
        END