C      PROGRAM FOR MATRIX MULTIPLICATION
        PROGRAM MATRIXMULT
C       defining Dimension of 3*3  
        DIMENSION MATa(3,3)
        INTEGER MATa,SUM
C       intializing value of sum as 0        
        SUM = 0
C 		ask the value for matrix A to user        
        WRITE (*,*) 'Enter number for matrix A (3x3):'
        DO 10 I=1,3
        DO 20 J=1,3
C 		reads value given by user to MATa        	
        READ *, MATa(I,J)
20    CONTINUE
10    CONTINUE

        DO 11 K=1,3
C 	replace the value of sum with diagonal element and current value of sum         	
        SUM = SUM +  MATa(K,K)
11    CONTINUE
        WRITE (*,*) 'Sum of main diagonal elements :',SUM
        END