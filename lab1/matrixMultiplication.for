C              PROGRAM FOR MULTIPLYING TWO 3*3 MATRIX        
                PROGRAM MULTIPLICATION
C               defining Dimension of 3*3  
                DIMENSION MATa(3,3),MATb(3,3), MATc(3,3)
                INTEGER MATA,MATB,MATc
C               take input for matrix A
                WRITE (*,*) 'Enter number for matrix A (3x3):'
                DO 11 I = 1,3
                DO 22 J = 1,3
                READ *, MATa(I,J)
22            CONTINUE
11            CONTINUE
C               take input for matrix B
                WRITE (*,*) 'Enter number for matrix B (3x3):'
                DO 33 I = 1,3
                DO 44 J = 1,3
                READ *, MATb(I,J)
44            CONTINUE
33            CONTINUE

                DO 55 K = 1,3
                DO 66 L = 1,3
C               setting matc(k,l) element as 0
                MATc (K,L) = 0
                DO 77 M = 1,3
C              replace the set MATc(k,l) i.e. 0 element with the multipled element of MATa (mata's row) and MATb (matb's column)  
               MATc(K,L) = MATc(K,L) +( MATa(K,M)*MATb(M,L))
77            CONTINUE
66            CONTINUE
55            CONTINUE
C               printing out the obtained matrix multiplication value
                WRITE (*,*) 'Multiplied matrix C (3x3):'
                DO 88 I = 1,3
                DO 99 J = 1,3
                WRITE (*,*) MATc(I,J)
99            CONTINUE
                WRITE (*,*)
88            CONTINUE
                END        