        PROGRAM SDMEAN

        INTEGER N
        REAL MEAN,SD,SUM,DIFF
        SUM = 0.0
        DIFF = 0.0
        WRITE (*,*) 'enter the nth number for mean'
        READ *, N
C      calculating mean value      
        DO 100 I = 1,N
        SUM = SUM+I
100     CONTINUE
        MEAN = SUM / N

C      calculating the value of standard deviation
        DO 200 I = 1,N
C      implementing the formula; diff is a temporary place holder for calculating value
        DIFF = DIFF + ((I - MEAN)**2.0)
200     CONTINUE
        SD =SQRT(DIFF /(N-1))

        WRITE (*,*) 'Mean is:',MEAN,'Strandard Deviation is:',SD
        END